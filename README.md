# Appmon

Appmon is a command line tool for capturing events from Apple&#39;s Endpoint Security Framework

Some of the code included with this project was taken from both the ProcessMonitor and FileMonitor projects created by Patrick Wardle:

https://github.com/objective-see/FileMonitor
https://github.com/objective-see/ProcessMonitor

Download the appmon app here: https://bitbucket.org/xorrior/appmon/downloads/appmon.zip

As of 01/28/20, the following event types are captured by Appmon:

ES_EVENT_TYPE_NOTIFY_EXEC

ES_EVENT_TYPE_NOTIFY_FORK

ES_EVENT_TYPE_NOTIFY_EXIT

ES_EVENT_TYPE_NOTIFY_MMAP

ES_EVENT_TYPE_NOTIFY_GET_TASK

ES_EVENT_TYPE_NOTIFY_CREATE

ES_EVENT_TYPE_NOTIFY_OPEN

ES_EVENT_TYPE_NOTIFY_WRITE

ES_EVENT_TYPE_NOTIFY_CLOSE

ES_EVENT_TYPE_NOTIFY_LINK

ES_EVENT_TYPE_NOTIFY_RENAME

ES_EVENT_TYPE_NOTIFY_KEXTLOAD

ES_EVENT_TYPE_NOTIFY_KEXTUNLOAD

ES_EVENT_TYPE_NOTIFY_IOKIT_OPEN

ES_EVENT_TYPE_NOTIFY_SETATTRLIST

ES_EVENT_TYPE_NOTIFY_SETEXTATTR

ES_EVENT_TYPE_NOTIFY_SETOWNER